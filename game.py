from random import randint

name = input("Hi! What is your name? ")
num_guesses = 5


for num in range(num_guesses):
    guess_month = randint(1, 12)
    guess_year = randint(1924, 2004)
    print("Guess: ", num, ":", name, "were you born in ", guess_month, "/", guess_year)
    answer = input("Answer: ")
    if answer == "yes":
        print("I knew it!")
        exit ()
    elif answer == "no":
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye")
        exit()
